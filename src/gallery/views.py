from itertools import islice

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, UpdateView, DeleteView
from django.views.generic.edit import CreateView

from .forms import PhotoForm
from .models import Photo


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'gallery/index.html'

    def dispatch(self, request, *args, **kwargs):
        has_perm = self.request.user.has_perm

        if (
            not has_perm('gallery.view_own_photos') and
            not has_perm('gallery.view_all_photos')
        ):
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        has_perm = self.request.user.has_perm

        if has_perm('gallery.view_all_photos'):
            photos = Photo.objects.all()
        elif has_perm('gallery.view_own_photos'):
            photos = Photo.objects.filter(owner=self.request.user)
        else:
            raise PermissionDenied()

        context = super(IndexView, self).get_context_data(**kwargs)

        photos = photos.iterator()
        rows = []
        while True:
            row = tuple(islice(photos, 3))
            rows.append(row)
            if len(row) < 3:
                break

        context.update({
            'rows': rows,
        })
        return context


class AddPhotoView(PermissionRequiredMixin, CreateView):
    permission_required = 'gallery.add_photo'
    raise_exception = True
    form_class = PhotoForm
    template_name = 'gallery/add.html'
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(AddPhotoView, self).form_valid(form)



class EditPhotoView(PermissionRequiredMixin, UpdateView):
    permission_required = 'gallery.change_photo'
    raise_exception = True
    form_class = PhotoForm
    model = Photo
    template_name = 'gallery/edit.html'
    success_url = reverse_lazy('index')


class DeletePhotoView(DeleteView):
    permission_required = 'gallery.delete_photo'
    raise_exception = True
    model = Photo
    template_name = 'gallery/confirm_delete.html'
    success_url = reverse_lazy('index')
