from django.apps import AppConfig


class GalleryConfig(AppConfig):
    name = 'gallery'
    app_label = __package__
